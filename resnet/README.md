# Общая информация об архитеткуре
![](../images/resnet_types.png)

![](../images/res_block.png)

# Детали реализации
> For each residual function, we use a stack of 3 layers. The three layers are 1×1, 3×3, and 1×1 convolutions, where the 1×1 layers are responsible for reducing and then increasing (restoring) dimensions, leaving the 3×3 layer a bottleneck with smaller input/output dimensions.

> ... for  the  same  output feature map size,  the layers have the same number of filters;  and if  the  feature  map  size  is  halved,  the  number  of  filters  is  doubled  so  as  to  preserve  the  time  complexity  per  layer.   We  perform  downsampling  directly  by convolutional layers that have a stride of 2.

> Downsampling is performed by conv3_1, conv4_1, and conv5_1 with a stride of 2.

>  When the dimensions increase, we consider two options:  (A) The shortcut still performs identity mapping, with extra zero entries padded for increasing dimensions. This option introduces no extraparameter; (B) The projection shortcut is used to match dimensions (done by 1×1 convolutions). For both options, when the shortcuts go across feature maps of two sizes, they are performed with a stride of 2.

> We adopt batchnormalization  (BN) right  after  each  convolution  andbefore activation.

# Другое
Данная реализация позволяет создавать вариации ResNet, состоящие из блоков по 3 сверчтоных слоя: ResNet-50, ResNet-101, ResNet-152 и т.д. 
