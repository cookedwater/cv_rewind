import torch
import torch.nn as nn
import torch.nn.functional as F

class ResNet(nn.Module):
    def __init__(self, n_classes, n_blocks = [3, 4, 6, 3]):
        super().__init__()
        self.body = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size = 7, stride = 2, padding = 3, bias = False),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size = 3, stride = 2, padding = 1),
            self.quarter(64, 64, nblocks = n_blocks[0], stride = 1),
            self.quarter(256, 128, nblocks = n_blocks[1], stride = 2),
            self.quarter(512, 256, nblocks = n_blocks[2], stride = 2),
            self.quarter(1024, 512, nblocks = n_blocks[3], stride = 2),
            nn.AdaptiveAvgPool2d((1,1))
        )
        
        self.fc = nn.Linear(2048, n_classes)
        
    def forward(self, x):
        x = self.body(x)
        x = x.flatten(1)
        x = self.fc(x)
        return x       
        
    def quarter(self, in_channels, channels, nblocks, stride):
        blocks = []
        for i, _ in enumerate(range(nblocks)):
            blocks.append(resBlock(in_channels, channels, stride**(i==0), dim_transform=i==0))
            in_channels = channels*4
        return nn.Sequential(*blocks)


class resBlock(nn.Module):
    def __init__(self, in_channels, channels, stride=1, dim_transform=False):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels, channels, kernel_size=1, stride=stride, bias=False)
        self.norm1 = nn.BatchNorm2d(channels)
        
        self.conv2 = nn.Conv2d(channels, channels, kernel_size=3, padding=1, bias=False)
        self.norm2 = nn.BatchNorm2d(channels)
        
        self.conv3 = nn.Conv2d(channels, channels*4, kernel_size=1, bias=False)
        self.norm3 = nn.BatchNorm2d(channels*4)
        self.relu = nn.ReLU(inplace=True)
        self.dim_transform = dim_transform
        if dim_transform:
            self.conv_transform = nn.Conv2d(in_channels, channels*4, kernel_size=1, stride=stride, bias=False)
            self.norm_transform = nn.BatchNorm2d(channels*4)
            
    def forward(self, x):
        x_in = x
        f = self.conv1(x)
        f = self.norm1(f)
        f = self.relu(f)
        f = self.conv2(f)
        f = self.norm2(f)
        f = self.relu(f)
        f = self.conv3(f)
        f = self.norm3(f)
        
        if self.dim_transform:
            x_in = self.conv_transform(x_in)
            x_in = self.norm_transform(x_in)
            
        f = f + x_in
        f = self.relu(f)
        return f

if __name__ == '__main__':
    model = ResNet(10)
    model.eval()
    t = torch.randn((5, 3, 224, 224))
    r = model(t)
