# Информация об архитектуре
> the net contains eight layers with weights; the first five are convolutional and the remaining three are fullyconnected. 

> The first convolutional layer filters the 224×224×3 input image with 96 kernels of size 11×11×3
with a stride of 4 pixels

> The second convolutional layer takes as input the (response-normalized
and pooled) output of the first convolutional layer and filters it with 256 kernels of size 5 × 5 × 48.

> The third convolutional layer has 384 kernels of size 3 × 3 ×
256 connected to the (normalized, pooled) outputs of the second convolutional layer.

>The fourth convolutional layer has 384 kernels of size 3 × 3 × 192 , and the fifth convolutional layer has 256 kernels of size 3 × 3 × 192, and the fifth convolutional layer has 256 kernels of size 3 × 3 × 192.

>  The fully-connected layers have 4096 neurons each.

# Другая важная информация
1. Параметры LRN: `k = 2, n = 5, α = 10−4, β = 0.75`. Применение: "*We applied this normalization after applying the ReLU nonlinearity...*"
2. *We use dropout in the first two fully-connected layers.*
3. Параметры max-pooling слоев: `s = 2, z = 3`
