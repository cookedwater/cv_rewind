import torch
import torch.nn as nn
import torch.nn.functional as F

class alexnet(nn.Module):
    def __init__(self, n_classes, p=0.5, input_padding=(2, 1, 1, 2)):
        super().__init__()
        self.padder = nn.ZeroPad2d(input_padding) # (padding_left ,padding_right ,padding_top,padding_bottom)
        self.conv_layers = nn.Sequential(
            nn.Conv2d(3, 96, kernel_size=11, stride=4), # 1-й
            nn.ReLU(inplace=True),
            nn.LocalResponseNorm(size=5, k=2),
            nn.MaxPool2d(3, stride=2),
            nn.Conv2d(96, 256, kernel_size=5, stride=1, padding=2), # 2-й (сохраняется размер изображения)
            nn.ReLU(inplace=True),
            nn.LocalResponseNorm(size=5, k=2),
            nn.MaxPool2d(3, stride=2),
            nn.Conv2d(256, 384, kernel_size=3, padding=1), # 3-й (сохраняется размер изображения)
            nn.Conv2d(384, 384, kernel_size=3, padding=1), # 4-й (сохраняется размер изображения)
            nn.Conv2d(384, 256, kernel_size=3, padding=1), # 5-й (сохраняется размер изображения)
            nn.MaxPool2d(3, stride=2)
        )
        
        self.fc_layers = nn.Sequential(
            nn.Dropout(p=p),
            nn.Linear(9216, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(p=p),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, n_classes)
        )
        
    def forward(self, x):
        if x.size(-1)==224:
            x = self.padder(x)
        x = self.conv_layers(x)
        x = torch.flatten(x, 1) 
        assert 9216==x.size(-1) # модель работает только на 227х227 изображениях
        x = self.fc_layers(x)
        return x 

if __name__ =='__main__':
    model = alexnet(100)
    model.eval()
    t1 = torch.randn((2, 3, 227,227))
    t2 = torch.randn((2, 3, 224,224))
    a, b = model(t1), model(t2)
