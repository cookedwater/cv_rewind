# Общая информация об архитектуре
![](../images/vgg_configs.png)

# Параметры сверточных и pooling слоев
> The image is passed through a stack of convolutional (conv.) layers, where we use filters with a very
small receptive field: 3 × 3 ... The convolution stride is
fixed to 1 pixel; the spatial padding of conv. layer input is such that the spatial resolution is preserved
after convolution, i.e. the padding is 1 pixel for 3 × 3 conv. layers. Spatial pooling is carried out by
five max-pooling layers, which follow some of the conv. layers (not all the conv. layers are followed
by max-pooling). Max-pooling is performed over a 2 × 2 pixel window, with stride 2.