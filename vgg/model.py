import torch
import torch.nn as nn
import torch.nn.functional as F

class vgg(nn.Module):
    def __init__(self, n_classes, config, pointwise=False, p=0.5):
        super().__init__()
        configs = {'A' : [1, 1, 2, 2, 2], 
           'B' : [2, 2, 2, 2, 2], 
           'D' : [2, 2, 3, 3, 3], 
           'E' : [2, 2, 4, 4, 4]}
        config = configs.get(config, None)
        assert config!=None
        self.conv_layers = self.get_convs(config, pointwise)
        self.fc_layers = nn.Sequential(
            nn.Linear(512*49, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(p=p),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(p=p),
            nn.Linear(4096, n_classes)
        )
        
    def forward(self, x):
        x = self.conv_layers(x)
        x = torch.flatten(x, 1)
        x = self.fc_layers(x)
        return x
        
    def get_convs(self, config, pointwise=False):
        layers = []
        in_channels = 3
        for i, num in enumerate(config):
            for _ in range(num):
                out_channels = min(512, 64*(2**i))
                layers+=[nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1), nn.ReLU(inplace=True)]
                in_channels = out_channels
            layers.append(nn.MaxPool2d(2, 2))
        if pointwise and sum(config)==13:  # версия D с 1x1 свертчоными слоями - версия C
            layers[14] = nn.Conv2d(256, 256, kernel_size=1)
            layers[21] = nn.Conv2d(512, 512, kernel_size=1)
            layers[28] = nn.Conv2d(512, 512, kernel_size=1)

        return nn.Sequential(*layers)

if __name__ == '__main__':
    t = torch.randn((4, 3, 224,224))
    for config in ['A', 'B', 'D', 'E']:
        model = vgg(10, config)
        model.eval()
        r = model(t)
    model = vgg(10, 'D', pointwise=True)
    model.eval()
    r = model(t)